-- calculate sens where cross-grading could have been the reason for churn
Drop Table if exists zone_bizops.rgc_cross_grade;
create table zone_bizops.rgc_cross_grade as
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain, 
                country,
                quarter,
                min(financial_year) as financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        group by 1,2,3,4,5
),
renewed_sens as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
upgraded_no_renew as (
        select  a.sen,
                max(b.date) as max_date
        from    land_sen as a
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Upgrade'
        and     b.date > a.min_date
        and     b.platform = 'Server'
        and     a.sen not in (select sen from renewed_sens)
        group by 1
        ),
combined_renewal as 
        (-- combine the results of renewed sens and upgraded-only sens
        select  sen,
                max_date
        from renewed_sens
        
        union
        
        select sen,
               max_date
        from upgraded_no_renew
        ),
land_churn_date as (
        select  sen, 
                (min_date + interval '365' DAY) as churn_date
        from land_sen
        where sen not in (select sen from combined_renewal) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + interval '365' DAY) as churn_date
        from    combined_renewal
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                a.country,
                b.base_product,
                count(distinct b.sen) as prod_count,
                max(c.churn_date) as churn_date
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain and a.country = b.tech_country
        left join land_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from land_churn_date)
        and     cast(b.paid_license_start_date as date) between c.churn_date - interval '90' DAY and c.churn_date + interval '90' DAY
        and     b.platform = 'Server'
        and     b.last_paid_license_level = 'Full'
        group by 1,2,3,4
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                a.country,
                b.base_product,
                count(distinct b.sen) as prod_count,
                max(c.churn_date) as churn_date
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain and a.country = b.tech_country
        left join renewed_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from renewed_churn_date)
        and     cast(b.paid_license_start_date as date) between c.churn_date - interval '90' DAY and c.churn_date + interval '90' DAY
        and     b.platform = 'Server'
        and     b.last_paid_license_level = 'Full'
        group by 1,2,3,4      
),
final_multi_sen_list as (
select   
        a.sen,
        a.base_product as land_product,
        b.base_product as cross_product,
        b.churn_date
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
   
union 

select   
        a.sen,
        a.base_product as land_product,
        b.base_product as cross_product,
        b.churn_date
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
)
select  a.churn_date,
        a.land_product,
        a.cross_product,  
        a.sen
from final_multi_sen_list as a
where a.land_product in ('JIRA','JIRA Core', 'JIRA Software', 'FishEye', 'Crucible','Bamboo')
and a.cross_product in ('Bitbucket', 'JIRA Software', 'JIRA Core', 'JIRA Service Desk')
and a.sen not in (select sen from zone_bizops.rgc_consolidation)
and a.sen not in (select sen from zone_bizops.rgc_migrations)
group by 1,2,3,4
order by 1,2,3,4
;

drop table if exists zone_bizops.rgc_cross_grade_final;
create table zone_bizops.rgc_cross_grade_final as
with removedata as 
(
select  month(churn_date) as churn_month,
        land_product,
        cross_product,
        case    when land_product in ('JIRA Software') and cross_product in ('JIRA Software','Bitbucket') then 0
                when land_product in ('JIRA') and cross_product in ('Bitbucket') then 0
                when land_product = 'Bamboo' and cross_product like '%JIRA%' then 0
                when land_product in ('FishEye','Crucible') and cross_product like '%JIRA%' then 0
        else cast(sen as integer)
        end as sen
from zone_bizops.rgc_cross_grade
where   year(churn_date) = 2016
and     month(churn_date) in (6,7,8)
group by 1,2,3,4
)
        select churn_month, cast(sen as varchar) as sen 
        from removedata
        where sen > 0
        
;