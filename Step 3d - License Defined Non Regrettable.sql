drop table if exists zone_bizops.rgc_downgrades;
create table zone_bizops.rgc_downgrades as

-- Calculates SENs that can be attributed to downgrades.

with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                country,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        group by 1,2,3,4
),
downgrade_samesen as (
        select  a.sen
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.platform = 'Server'
        and     b.license_level = 'Starter'
        and     b.date >= a.min_date
        group by 1
),
renewed_sens as (
        select  a.sen,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        group by 1
        order by 1
),
upgraded_no_renew as (
        select  a.sen,
                max(b.date) as max_date
        from    land_sen as a
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Upgrade'
        and     b.date > a.min_date
        and     b.platform = 'Server'
        and     a.sen not in (select sen from renewed_sens)
        group by 1
        ),
combined_renewal as 
        (-- combine the results of renewed sens and upgraded-only sens
        select  sen,
                max_date
        from renewed_sens
        
        union
        
        select  sen,
                max_date
        from upgraded_no_renew
        ),
starter_land as (
        select  sen, 
                email_domain,
                country,
                base_product,
                min(date) as min_start
        from public.sale
        where   sale_type = 'Starter'
        and     platform = 'Server'
        group by 1,2,3,4
),
renewed_starter as (
        select  a.sen as affected_sen,
                a.email_domain,
                a.country
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain and a.country = b.country
        left join combined_renewal as c on a.sen = c.sen
        where   a.base_product = b.base_product
        and     b.min_start > c.max_date + interval '275' DAY
        and     b.min_start < c.max_date + interval '455' DAY
        group by 1,2,3
        order by 1
),
non_renewed_starter as (
        select a.sen as affected_sen,
        a.email_domain,
        a.country
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain and a.country = b.country
        where   a.base_product = b.base_product
        and     b.min_start > a.min_date + interval '275' DAY
        and     b.min_start < a.min_date + interval '455' DAY
        and     a.sen not in (select sen from combined_renewal)
        group by 1,2,3
        order by 1,2
),
active_prod as (
        select  b.tech_email_domain,
                b.tech_country as country,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain and a.country = b.tech_country
        left join starter_land as c on a.email_domain = c.email_domain and a.base_product = c.base_product and a.country = c.country
        where   b.last_license_level = 'Full'
        and     cast(b.paid_license_end_date as date) < c.min_start
        and     cast(b.paid_license_end_date as date) > c.min_start - interval '90' DAY
        group by 1,2,3
),
final_sen_downgrade_new as (
        select  distinct a.affected_sen
        from    renewed_starter as a
        join    active_prod as c on a.email_domain = c.tech_email_domain and a.country = c.country
        and     a.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1

union
        
        select  distinct b.affected_sen
        from    non_renewed_starter as b
        join    active_prod as c on b.email_domain = c.tech_email_domain  and b.country = c.country
        and     b.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1
        group by 1
        order by 1
),
final_cohort as
(
        select  a.affected_sen as sen
        from    final_sen_downgrade_new as a
        left join land_sen as b on a.affected_sen=b.sen
        group by 1
        
        union
        
        select a.sen as sen
        from downgrade_samesen as a
        left join land_sen as b on a.sen = b.sen
        group by 1
)
select  b.paid_license_end_date, 
        a.sen
        from final_cohort as a
        left join public.license as b on a.sen = b.sen
        where  b.paid_license_end_date between '2016-06-01' and '2016-08-31'   
        and a.sen not in (select sen from zone_bizops.rgc_consolidation)
        and a.sen not in (select sen from zone_bizops.rgc_migrations)
        and a.sen not in (select sen from zone_bizops.rgc_cross_grade_final)
        
        ;
        
      
