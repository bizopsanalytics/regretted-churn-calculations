--Churn of primary products
select
        dd.calendar_year_month,
        b.base_product
        --,sum(case when et.name in ('New','Domain Change (New)')  then 1 else 0 end) as "New"
        ,sum(case when et.name in ('Churn','Domain Change (Churn)') then -1 else 0 end) as "Churn"
        --,sum(case when et.name = 'Return' then 1 else 0 end) as "Return"
from
        model.fact_license_event as a
        join model.dim_license_event_type et using (license_event_type_id)
        join model.dim_date dd using (date_id)
        join model.dim_product as b on a.product_id = b.product_id
where
        a.date_id between 20160531 and 20160831
and     b.platform = 'Server'
and     b.base_product not in (
                                'Bamboo',
                                'Bitbucket',
                                'Confluence',
                                'Crucible',
                                'FishEye',
                                'HipChat',
                                'JIRA',
                                'JIRA Core',
                                'JIRA Service Desk',
                                'JIRA Software'  
                            )
group by 1,2
order by 1,2;

