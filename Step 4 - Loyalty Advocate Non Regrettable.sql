select  
        month(expiry_date) as expiry_month,
        --product,
        stage_reason,
        count(distinct sen) as sen_count
from zone_c4l.renewals_lost
where stage_reason in (
                        'Using another instance (same product)',
                        'Consolidating Instances',
                        'Cross-graded to JIRA Core',
                        'Moved to Data Center',
                        'Project Completed',
                        'Company Out of Business',
                        'Downgrade to Starter',
                        'Invalid Opportunity',
                        'Moved to another BTF product',
                        'Moved to Cloud'
                       )

and year(expiry_date) = 2016
and month(expiry_date) in (6,7,8)
and sen not in (select sen from zone_bizops.rgc_consolidation)
and sen not in (select sen from zone_bizops.rgc_migrations)
and sen not in (select sen from zone_bizops.rgc_cross_grade_final)
and sen not in (select sen from zone_bizops.rgc_downgrades)
group by 1,2
order by 1,2

