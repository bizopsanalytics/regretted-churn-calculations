-- check for the count of consolidation.
with license_history as
(--identify Full Server license ownership during a period.
        select  a.customer_id,
                a.product_id,
                a.license_id,
                c.smart_domain, 
                b.base_product,
                case    when b.family like '%JIRA%' 
                        then b.family
                        else b.base_product
                end as family_group,
                d.unit_count,      
                a.date_id,
                d.sen,
                e.tech_country
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_customer as c on a.customer_id = c.customer_id
        join    model.dim_license as d on a.license_id = d.license_id
        join    public.license as e on d.sen = e.sen
        where   b.platform in ('Server','Data Center')
        and     a.date_id > 20160531
        and     d.level = 'Full'
        and     b.base_product in (     'JIRA', 
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'JIRA Core',
                                        'Confluence',
                                        'Bamboo',
                                        'HipChat',
                                        'Bitbucket',
                                        'Crucible',
                                        'FishEye'
                                  )
),
last_active_date as 
( --find the latest active date of each sen, i.e. the churn date.
        select  sen,
                max(date_id) as max_date
        from license_history
        group by 1
),
churned_cohort as 
(-- create the cohort of customers of churned licenses for further analysis.
        select  a.*, 
                b.max_date 
        from    license_history as a
        left join last_active_date as b on a.sen = b.sen
        where   b.max_date < 20160831
),
ownership as 
( --identify the number of products owned within each family at each of the dates the customer is active.           
        select  ch.date_id,
                ch.smart_domain,
                ch.tech_country,
                ch.family_group,
                ch.unit_count,
                count(ch.license_id) as prod_count
        from    license_history as ch
        group by 1,2,3,4,5
),
consolidation_cohort as 
(--final list of sens that churned due to Consolidation

        select    a.sen, 
                  a.base_product,
                  max(max_date) as churn_date
        from      churned_cohort as a
        left join ownership as b on  a.smart_domain = b.smart_domain 
                         and a.tech_country = b.tech_country
                         and a.family_group = b.family_group
                         and a.date_id = b.date_id
        where     b.prod_count > 1
        and       b.unit_count >= a.unit_count
        group by  1,2
 )       
        select  case 
                when a.churn_date < 20160701 then '2016_06'
                when a.churn_date < 20160801 then '2016_07'
                when a.churn_date < 20160901 then '2016_08'
                else 'Other'
                end as churn_month,
                b.stage_reason, 
                count(a.sen) as count_consolidated
        from consolidation_cohort as a
        left join zone_c4l.renewals_lost as b on a.sen = b.sen 
        --where lost_date is not null
        group by 1,2
        ;
        
Drop table if exists zone_bizops.rgc_consolidation;
Create table zone_bizops.rgc_consolidation as

with license_history as
(--identify Full Server license ownership during a period.
        select  a.customer_id,
                a.product_id,
                a.license_id,
                c.smart_domain, 
                b.base_product,
                case    when b.family like '%JIRA%' 
                        then b.family
                        else b.base_product
                end as family_group,
                d.unit_count,      
                a.date_id,
                d.sen,
                e.tech_country
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_customer as c on a.customer_id = c.customer_id
        join    model.dim_license as d on a.license_id = d.license_id
        join    public.license as e on d.sen = e.sen
        where   b.platform in ('Server','Data Center')
        and     a.date_id > 20160531
        and     d.level = 'Full'
        and     b.base_product in (     'JIRA', 
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'JIRA Core',
                                        'Confluence',
                                        'Bamboo',
                                        'HipChat',
                                        'Bitbucket',
                                        'Crucible',
                                        'FishEye'
                                  )
),
last_active_date as 
( --find the latest active date of each sen, i.e. the churn date.
        select  sen,
                max(date_id) as max_date
        from license_history
        group by 1
),
churned_cohort as 
(-- create the cohort of customers of churned licenses for further analysis.
        select  a.*, 
                b.max_date 
        from    license_history as a
        left join last_active_date as b on a.sen = b.sen
        where   b.max_date < 20160831
),
ownership as 
( --identify the number of products owned within each family at each of the dates the customer is active.           
        select  ch.date_id,
                ch.smart_domain,
                ch.tech_country,
                ch.family_group,
                ch.unit_count,
                count(ch.license_id) as prod_count
        from    license_history as ch
        group by 1,2,3,4,5
),
consolidation_cohort as 
(--final list of sens that churned due to Consolidation

        select    a.sen, 
                  a.base_product,
                  max(max_date) as churn_date
        from      churned_cohort as a
        left join ownership as b on  a.smart_domain = b.smart_domain 
                         and a.tech_country = b.tech_country
                         and a.family_group = b.family_group
                         and a.date_id = b.date_id
        where     b.prod_count > 1
        and       b.unit_count >= a.unit_count
        group by  1,2
 )       
        select  b.stage_reason,
                a.churn_date,
                a.sen
        from consolidation_cohort as a
        left join zone_c4l.renewals_lost as b on a.sen = b.sen 
        group by 1,2,3
  ;
  
 
  select 
                case 
                when churn_date < 20160701 then '2016_06'
                when churn_date < 20160801 then '2016_07'
                when churn_date < 20160901 then '2016_08'
                else 'Other'
                end as churn_month,
                count(sen)
