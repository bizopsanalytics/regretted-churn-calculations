with loyalty_regret as 
(
select  
        year(expiry_date) as expiry_year,
        month(expiry_date) as expiry_month,
        product,
        stage_reason,
        sen
from zone_c4l.renewals_lost
where stage_reason in (    
                        'Not satisfied with Support / Updates',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Reason not given',
                        'Too expensive / No budget',
                        'No Response',
                        'Invalid Contact Information',
                        'JIRA Renaissance Package / Pricing',
                        'Other/no additional information given'
                       )
or stage_reason like '%Using Competitor%'
)
select  expiry_year, 
        expiry_month, 
        stage_reason, 
        count(distinct sen) as sen_count
from loyalty_regret
where expiry_year = 2016
and expiry_month in (6,7,8)
and sen not in (select sen from zone_bizops.rgc_consolidation)
and sen not in (select sen from zone_bizops.rgc_migrations)
and sen not in (select sen from zone_bizops.rgc_cross_grade_final)
and sen not in (select sen from zone_bizops.rgc_downgrades)
group by 1,2,3
order by 2 asc